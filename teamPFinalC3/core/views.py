from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'core/index.html')

def acercade(request):
    return render(request, 'core/acercade.html')

def contacto(request):
    return render(request, 'core/contacto.html')